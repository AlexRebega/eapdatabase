﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace LaboratorElectronica
{
    class DBconnect
    {
    public MySqlConnection connection;
    private string server;
    private string database;
    private string uid;
    private string password;

    //Constructor
    public DBconnect()
    {
        Initialize();
    }

    //Initialize values
    private void Initialize()
    {
        server = "localhost";
        database = "laboratorelectronica";
        uid = "root";
        password = "1234";
        string connectionString;
        connectionString = "SERVER=" + server + ";" + "DATABASE=" + 
		database + ";" + "UID=" + uid + ";" + "PASSWORD=" + password + ";";
        connection = new MySqlConnection(connectionString);
        
    }

    //open connection to database
    public bool OpenConnection()
    {
        try
        {
            connection.Open();
            //MessageBox.Show("Conectare reusita ! Dam petrecere ! "); // this line was used for debug
            return true;
        }
        catch (MySqlException ex)
        {
            //When handling errors, you can your application's response based 
            //on the error number.
            //The two most common error numbers when connecting are as follows:
            //0: Cannot connect to server.
            //1045: Invalid user name and/or password.
            switch (ex.Number)
            {
                case 0:
                    MessageBox.Show("Cannot connect to server.  Contact administrator");
                    break;

                case 1045:
                    MessageBox.Show("Invalid username/password, please try again");
                    break;
            }
            return false;
        }
    }

    //Close connection
    public bool CloseConnection()
    {
        try
        {
            connection.Close();
            return true;
        }
        catch (MySqlException ex)
        {
            MessageBox.Show(ex.Message);
            return false;
        }
    }
    
    //Used for insert,update, delete
    public void execute_command(string query)
    {
        
        //open connection
        if (this.OpenConnection() == true)
        {
            try
            {
                //create command and assign the query and connection from the constructor
                MySqlCommand cmd = new MySqlCommand(query, connection);

                //Execute command
                cmd.ExecuteNonQuery();

                //close connection
                this.CloseConnection();
            }
            catch(MySqlException ex)
            {
                MessageBox.Show("Interogarea MySQL a generat o eroare:" +ex.ToString() + "  Verificati integritatea datelor introduse !");

            }
        }
    }
/*
    //Update statement
    public void Update(string query)
    {

        //Open connection
        if (this.OpenConnection() == true)
        {
            try
            {
                //create mysql command
                MySqlCommand cmd = new MySqlCommand();
                //Assign the query using CommandText
                cmd.CommandText = query;
                //Assign the connection using Connection
                cmd.Connection = connection;

                //Execute query
                cmd.ExecuteNonQuery();

                //close connection
                this.CloseConnection();
            }
            catch (MySqlException ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
    }

    //Delete statement
    public void Delete()
    {
        string query = "DELETE FROM tableinfo WHERE name='John Smith'";

        if (this.OpenConnection() == true)
        {
            MySqlCommand cmd = new MySqlCommand(query, connection);
            cmd.ExecuteNonQuery();
            this.CloseConnection();
        }
    }
*/
    // for a selected entry, return ID(int32)
    public int ReadID(string query)
    {
        int tmp = -1;
        MySqlCommand myCommand = new MySqlCommand(query, connection);
        connection.Open();
        try
        {
            MySqlDataReader myReader = myCommand.ExecuteReader();
            // Always call Read before accessing data. 
            while (myReader.Read())
            {
                tmp = myReader.GetInt32(0);
                //MessageBox.Show(tmp.ToString()); //debug
            }
            // always call Close when done reading. 
            myReader.Close();
            // Close the connection when done with it. 
        }
    
        finally
        {
            connection.Close();
        }
        return tmp;
    }


    //Select statement
    public List<string>[] Select()
    {
        string query = "SELECT * FROM tableinfo";

        //Create a list to store the result
        List<string>[] list = new List<string>[3];
        list[0] = new List<string>();
        list[1] = new List<string>();
        list[2] = new List<string>();

        //Open connection
        if (this.OpenConnection() == true)
        {
            //Create Command
            MySqlCommand cmd = new MySqlCommand(query, connection);
            //Create a data reader and Execute the command
            MySqlDataReader dataReader = cmd.ExecuteReader();

            //Read the data and store them in the list
            while (dataReader.Read())
            {
                list[0].Add(dataReader["id"] + "");
                list[1].Add(dataReader["name"] + "");
                list[2].Add(dataReader["age"] + "");
            }

            //close Data Reader
            dataReader.Close();

            //close Connection
            this.CloseConnection();

            //return list to be displayed
            return list;
        }
        else
        {
            return list;
        }
    }

    //Count statement
    public int Count()
    {
        string query = "SELECT Count(*) FROM tableinfo";
        int Count = -1;

        //Open Connection
        if (this.OpenConnection() == true)
        {
            //Create Mysql Command
            MySqlCommand cmd = new MySqlCommand(query, connection);

            //ExecuteScalar will return one value
            Count = int.Parse(cmd.ExecuteScalar() + "");

            //close Connection
            this.CloseConnection();

            return Count;
        }
        else
        {
            return Count;
        }
    }



    //TODO : http://www.codeproject.com/Articles/43438/Connect-C-to-MySQL
    //Backup
    public void Backup()
    {
    }

    //Restore
    public void Restore()
    {
    }
}
}
