﻿namespace LaboratorElectronica
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.vizualizareToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.membriToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.proiecteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.echipeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.resurseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.resurseConsumateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.complexQuerryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.proiecteScumpeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.costProiecteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.membriFaraAccesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.coordonatoriToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.situatieEchipeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.situatieProiecteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Right;
            this.dataGridView1.Location = new System.Drawing.Point(307, 24);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(501, 459);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            this.dataGridView1.RowValidated += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            this.dataGridView1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.dataGridView1_MouseClick);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(78, 27);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "Insert";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(78, 56);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 1;
            this.button2.Text = "Update";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(78, 85);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 1;
            this.button3.Text = "Delete";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.vizualizareToolStripMenuItem,
            this.complexQuerryToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(808, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // vizualizareToolStripMenuItem
            // 
            this.vizualizareToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.membriToolStripMenuItem,
            this.proiecteToolStripMenuItem,
            this.echipeToolStripMenuItem,
            this.resurseToolStripMenuItem,
            this.resurseConsumateToolStripMenuItem});
            this.vizualizareToolStripMenuItem.Name = "vizualizareToolStripMenuItem";
            this.vizualizareToolStripMenuItem.Size = new System.Drawing.Size(74, 20);
            this.vizualizareToolStripMenuItem.Text = "Vizualizare";
            // 
            // membriToolStripMenuItem
            // 
            this.membriToolStripMenuItem.Name = "membriToolStripMenuItem";
            this.membriToolStripMenuItem.Size = new System.Drawing.Size(176, 22);
            this.membriToolStripMenuItem.Text = "Membri";
            this.membriToolStripMenuItem.Click += new System.EventHandler(this.membriToolStripMenuItem_Click);
            // 
            // proiecteToolStripMenuItem
            // 
            this.proiecteToolStripMenuItem.Name = "proiecteToolStripMenuItem";
            this.proiecteToolStripMenuItem.Size = new System.Drawing.Size(176, 22);
            this.proiecteToolStripMenuItem.Text = "Proiecte";
            this.proiecteToolStripMenuItem.Click += new System.EventHandler(this.proiecteToolStripMenuItem_Click);
            // 
            // echipeToolStripMenuItem
            // 
            this.echipeToolStripMenuItem.Name = "echipeToolStripMenuItem";
            this.echipeToolStripMenuItem.Size = new System.Drawing.Size(176, 22);
            this.echipeToolStripMenuItem.Text = "Echipe";
            this.echipeToolStripMenuItem.Click += new System.EventHandler(this.echipeToolStripMenuItem_Click);
            // 
            // resurseToolStripMenuItem
            // 
            this.resurseToolStripMenuItem.Name = "resurseToolStripMenuItem";
            this.resurseToolStripMenuItem.Size = new System.Drawing.Size(176, 22);
            this.resurseToolStripMenuItem.Text = "Resurse disponibile";
            this.resurseToolStripMenuItem.Click += new System.EventHandler(this.resurseToolStripMenuItem_Click);
            // 
            // resurseConsumateToolStripMenuItem
            // 
            this.resurseConsumateToolStripMenuItem.Name = "resurseConsumateToolStripMenuItem";
            this.resurseConsumateToolStripMenuItem.Size = new System.Drawing.Size(176, 22);
            this.resurseConsumateToolStripMenuItem.Text = "Resurse consumate";
            this.resurseConsumateToolStripMenuItem.Click += new System.EventHandler(this.resurseConsumateToolStripMenuItem_Click);
            // 
            // complexQuerryToolStripMenuItem
            // 
            this.complexQuerryToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.proiecteScumpeToolStripMenuItem,
            this.costProiecteToolStripMenuItem,
            this.membriFaraAccesToolStripMenuItem,
            this.coordonatoriToolStripMenuItem,
            this.situatieEchipeToolStripMenuItem,
            this.situatieProiecteToolStripMenuItem});
            this.complexQuerryToolStripMenuItem.Name = "complexQuerryToolStripMenuItem";
            this.complexQuerryToolStripMenuItem.Size = new System.Drawing.Size(102, 20);
            this.complexQuerryToolStripMenuItem.Text = "ComplexQuerry";
            this.complexQuerryToolStripMenuItem.Click += new System.EventHandler(this.complexQuerryToolStripMenuItem_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Location = new System.Drawing.Point(307, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 15);
            this.label1.TabIndex = 3;
            this.label1.Text = "Date";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(129, 134);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 20);
            this.textBox1.TabIndex = 4;
            this.textBox1.Visible = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(27, 141);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "label2";
            this.label2.Visible = false;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(129, 164);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(100, 20);
            this.textBox2.TabIndex = 4;
            this.textBox2.Visible = false;
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(129, 190);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(100, 20);
            this.textBox3.TabIndex = 4;
            this.textBox3.Visible = false;
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(129, 216);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(100, 20);
            this.textBox4.TabIndex = 4;
            this.textBox4.Visible = false;
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(129, 242);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(100, 20);
            this.textBox5.TabIndex = 4;
            this.textBox5.Visible = false;
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(129, 268);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(100, 20);
            this.textBox6.TabIndex = 4;
            this.textBox6.Visible = false;
            // 
            // textBox7
            // 
            this.textBox7.Location = new System.Drawing.Point(129, 297);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(100, 20);
            this.textBox7.TabIndex = 4;
            this.textBox7.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(27, 171);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "label2";
            this.label3.Visible = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(27, 197);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "label2";
            this.label4.Visible = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(27, 223);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(35, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "label2";
            this.label5.Visible = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(27, 249);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(35, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "label2";
            this.label6.Visible = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(27, 275);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(35, 13);
            this.label7.TabIndex = 5;
            this.label7.Text = "label2";
            this.label7.Visible = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(27, 304);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(35, 13);
            this.label8.TabIndex = 5;
            this.label8.Text = "label2";
            this.label8.Visible = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(0, 339);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(301, 144);
            this.pictureBox1.TabIndex = 6;
            this.pictureBox1.TabStop = false;
            // 
            // proiecteScumpeToolStripMenuItem
            // 
            this.proiecteScumpeToolStripMenuItem.Name = "proiecteScumpeToolStripMenuItem";
            this.proiecteScumpeToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
            this.proiecteScumpeToolStripMenuItem.Text = "Proiecte Scumpe";
            this.proiecteScumpeToolStripMenuItem.Click += new System.EventHandler(this.proiecteScumpeToolStripMenuItem_Click);
            // 
            // costProiecteToolStripMenuItem
            // 
            this.costProiecteToolStripMenuItem.Name = "costProiecteToolStripMenuItem";
            this.costProiecteToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
            this.costProiecteToolStripMenuItem.Text = "Cost Proiecte";
            this.costProiecteToolStripMenuItem.Click += new System.EventHandler(this.costProiecteToolStripMenuItem_Click);
            // 
            // membriFaraAccesToolStripMenuItem
            // 
            this.membriFaraAccesToolStripMenuItem.Name = "membriFaraAccesToolStripMenuItem";
            this.membriFaraAccesToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
            this.membriFaraAccesToolStripMenuItem.Text = "Membri fara acces";
            this.membriFaraAccesToolStripMenuItem.Click += new System.EventHandler(this.membriFaraAccesToolStripMenuItem_Click);
            // 
            // coordonatoriToolStripMenuItem
            // 
            this.coordonatoriToolStripMenuItem.Name = "coordonatoriToolStripMenuItem";
            this.coordonatoriToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
            this.coordonatoriToolStripMenuItem.Text = "Coordonatori";
            this.coordonatoriToolStripMenuItem.Click += new System.EventHandler(this.coordonatoriToolStripMenuItem_Click);
            // 
            // situatieEchipeToolStripMenuItem
            // 
            this.situatieEchipeToolStripMenuItem.Name = "situatieEchipeToolStripMenuItem";
            this.situatieEchipeToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
            this.situatieEchipeToolStripMenuItem.Text = "Situatie Echipe";
            this.situatieEchipeToolStripMenuItem.Click += new System.EventHandler(this.situatieEchipeToolStripMenuItem_Click);
            // 
            // situatieProiecteToolStripMenuItem
            // 
            this.situatieProiecteToolStripMenuItem.Name = "situatieProiecteToolStripMenuItem";
            this.situatieProiecteToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
            this.situatieProiecteToolStripMenuItem.Text = "Situatie Proiecte";
            this.situatieProiecteToolStripMenuItem.Click += new System.EventHandler(this.situatieProiecteToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(808, 483);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBox7);
            this.Controls.Add(this.textBox6);
            this.Controls.Add(this.textBox5);
            this.Controls.Add(this.textBox4);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem vizualizareToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem membriToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem proiecteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem echipeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem resurseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem resurseConsumateToolStripMenuItem;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ToolStripMenuItem complexQuerryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem proiecteScumpeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem costProiecteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem membriFaraAccesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem coordonatoriToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem situatieEchipeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem situatieProiecteToolStripMenuItem;

    }
}

