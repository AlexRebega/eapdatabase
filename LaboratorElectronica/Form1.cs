﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

/*TODO list
 * 
 * 
 * adauga cod pt backup/restore in DBconnect.cs + butoane interfata
 * public : OpenConnection, CloseConnection, connection - gaseste metodade a le face private
 * 
 * update si delete pentru echipe si resurse consumate e naspa
 * 
 * intr-o zi cu soare, adauga o tabela de pontaj in baza de date...
 * 
*/

namespace LaboratorElectronica
{
    public partial class Form1 : Form
    {
        private DBconnect DBConnect;
        private MySqlDataAdapter mySqlDataAdapter;
        string query;
        string[] info = new string[7];
        int table_select = 0;
        int id = -1;
        string[] temp;

        public Form1()
        {
            InitializeComponent();
            DBConnect = new DBconnect();

        }

        //makes possible to commit changes to database made in the dataGridView interface 
        private void dataGridView1_RowValidated(object sender, DataGridViewCellEventArgs e)
        {
             DataTable changes = ((DataTable)dataGridView1.DataSource).GetChanges();
 
                if (changes != null)
                {
                    MySqlCommandBuilder mcb = new MySqlCommandBuilder(mySqlDataAdapter);
                    mySqlDataAdapter.UpdateCommand = mcb.GetUpdateCommand();
                    mySqlDataAdapter.Update(changes);
                    ((DataTable)dataGridView1.DataSource).AcceptChanges();
                }           
        }
        // THIS CODE DOES SEEM TO NOT ACTUALLY WORK :)
        // but we were supposed to implement INSER, UPDATE, DELETE anyway so ...

        private void Form1_Load(object sender, EventArgs e)
        {

        }
        //insert
        private void button1_Click(object sender, EventArgs e)
        {
            switch (table_select)
            {
                case 0:
                    MessageBox.Show("Nu ati selectat o tabela in care sa introduceti date");
                    break;
                case 1:
                    // Insert in `membri`
                    //collect information from textBoxes
                    info[0] = textBox1.Text;
                    info[1] = textBox2.Text;
                    info[2] = textBox3.Text;
                    info[3] = textBox4.Text;
                    info[4] = textBox5.Text;
                    info[5] = textBox6.Text;
                    info[6] = textBox7.Text;
                    // check data 
                    if ((info[0] == "") || info[1] == "")
                        MessageBox.Show("Nume Incomplet !");
                    else
                    {
                        // Insert entry
                        query = "INSERT INTO `membri` (`ID`, `Nume`, `Prenume`, `Facultatea`, `Anul`, `Cod_acces`, `Mail`, `NrTelefon`) VALUES (NULL, '" + info[0] + "','" + info[1] + "','" + info[2] +
                            "','" + info[3] + "','" + info[4] + "','" + info[5] + "','" + info[6] + "');";
                        
                        DBConnect.execute_command(query);
                        MessageBox.Show("Insert reusit !");
                        
                        //Empty text filds
                        textBox1.Text = "";
                        textBox2.Text = "";
                        textBox3.Text = "";
                        textBox4.Text = "";
                        textBox5.Text = "";
                        textBox6.Text = "";
                        textBox7.Text = "";
                        //MessageBox.Show(query);   // for debug
                    }

                    break;
                case 2:
                    // insert into `proiecte`
                    // collect info from textboxes
                    info[0] = textBox1.Text;
                    info[1] = textBox2.Text;
                    info[2] = textBox3.Text;

                    if ((info[0] == "") || info[1] == "")
                        MessageBox.Show("ID Coordonator si numele proiectului nu pot lipsi!");
                    else
                    {
                        // Insert entry
                        query = @"INSERT INTO `proiecte` (`ID_Proiect`, `Coordonator`, `Proiect`, `Buget`) VALUES (NULL,
                                (SELECT ID FROM `membri` WHERE nume ='" + info[0] + "'),'" + info[1] + "','" + info[2] + "');";

                        DBConnect.execute_command(query);
                        //MessageBox.Show(query); //debug
                        MessageBox.Show("Insert reusit !");

                        //Empty text filds
                        textBox1.Text = "";
                        textBox2.Text = "";
                        textBox3.Text = "";
                    }
                    break;
                case 3:

                    // insert into `echipe`
                    // collect info from textboxes
                    info[0] = textBox1.Text;
                    info[1] = textBox2.Text;

                    if ((info[0] == "") || info[1] == "")
                        MessageBox.Show(@"Pentru a insera un membru intr-o echipa introduceti 
                        identificatorul membrului si identificatorul proiectului");
                    else
                    {
                        // Insert entry
                        query = @"INSERT INTO `echipe` (`ID_Membru`, `ID_Proiect`) 
                                VALUES ((SELECT ID FROM `membri` WHERE
                                nume = '" + info[0] + "'),(SELECT ID_Proiect FROM `proiecte` WHERE proiect ='" + info[1] + "'));";

                        DBConnect.execute_command(query);
                        //MessageBox.Show(query); //debug
                        MessageBox.Show("Insert reusit !");

                        //Empty text filds
                        textBox1.Text = "";
                        textBox2.Text = "";
                        textBox3.Text = "";
                    }

                    break;
                case 4:
                    // Insert in `resurse`
                    //collect information from textBoxes
                    info[0] = textBox1.Text;
                    info[1] = textBox2.Text;
                    info[2] = textBox3.Text;
                    info[3] = textBox4.Text;
                    info[4] = textBox5.Text;
                    info[5] = textBox6.Text;
                    info[6] = textBox7.Text;
                    // check data 
                    if (info[0] == "")
                        MessageBox.Show("Nu puteti introduce o piesa fara nume!");
                    else
                    {
                        // Insert entry
                        query = @"INSERT INTO `resurse` (`ID_Piesa`, `Nume_piesa`, `Cantitate_disponibila`, `Producator`, `Fisa_tehnica`, `Distribuitor`, 
                            `Pret`) VALUES (NULL, '" + info[0] + "','" + info[1] + "','" + info[2] +
                            "','" + info[3] + "','" + info[4] + "','" + info[5] + "');";

                        DBConnect.execute_command(query);
                        //MessageBox.Show(query); //debug
                        MessageBox.Show("Insert reusit !");

                        //Empty text filds
                        textBox1.Text = "";
                        textBox2.Text = "";
                        textBox3.Text = "";
                        textBox4.Text = "";
                        textBox5.Text = "";
                        textBox6.Text = "";
                        textBox7.Text = "";
                        //MessageBox.Show(query);   // for debug
                    }
                    break;

            case 5:
                    // insert into `resurse consumate`
                    // collect info from textboxes
                    info[0] = textBox1.Text;
                    info[1] = textBox2.Text;
                    info[2] = textBox3.Text;

                    if ((info[0] == "") || (info[1] == "")||(info[2] == "") )
                        MessageBox.Show("Datele incomplete !");
                    else
                    {
                        // Insert entry
                        query = @"INSERT INTO `resurse_consumate` (`ID_Proiect`, `ID_piesa`, `Cantitate`) 
                                VALUES ((SELECT ID_Proiect FROM `proiecte` WHERE proiect = '" + info[0] + "'),(SELECT ID_Piesa FROM `resurse` WHERE Nume_piesa ='" + info[1] + "'),'" + info[2] + "');";

                        DBConnect.execute_command(query);
                        //MessageBox.Show(query); //debug
                        MessageBox.Show("Insert reusit !");

                        //Empty text filds
                        textBox1.Text = "";
                        textBox2.Text = "";
                        textBox3.Text = "";
                    }
                    break;

            }
        }

        //update
        private void button2_Click(object sender, EventArgs e)
        {
            
             // UPDATE table_name SET column1=value, column2=value2,...WHERE some_column=some_value
            //UPDATE `membri` SET Nume = "Ionescu" WHERE ID = 13 
            switch (table_select)
            {
                case 0:
                    MessageBox.Show("Selectati o tabela !");
                    break;
                case 1:
                    // UPDATE with info from user, where ID = id (id selected by user clicking a row)
                    query = @"UPDATE `membri` SET nume = '" + textBox1.Text +
                        "', prenume = '" + textBox2.Text + "',facultatea = '" + textBox3.Text + "', anul = '"
                        + textBox4.Text + "', cod_acces = '" + textBox5.Text + "', mail = '" + textBox6.Text +
                        "', nrtelefon = '" + textBox7.Text + "' WHERE ID = " + id.ToString();

                        DBConnect.execute_command(query);
                        //MessageBox.Show(query); //debug
                        MessageBox.Show("Update reusit !");
                    break;

                case 2:
                    query = @"UPDATE `proiecte` SET `Coordonator` = 
                            (SELECT ID FROM `membri` WHERE nume ='" + textBox1.Text + "'), Proiect = '" + textBox2.Text + "', Buget = '" + textBox3.Text +"' WHERE ID_Proiect = " + id.ToString() + ";";

                        DBConnect.execute_command(query);
                        //MessageBox.Show(query); //debug
                        MessageBox.Show("Update reusit !");

                    break;

                case 3:
                    query = @"UPDATE `echipe` SET ID_membru = 
(SELECT ID from `membri` WHERE nume = '" + temp[0] + "' AND prenume = '" + temp[1] + "'), ID_Proiect = (SELECT ID_Proiect FROM `proiecte` WHERE proiect = '" + textBox2.Text + "')";
                    //MessageBox.Show(query); //debug

                    DBConnect.execute_command(query);
                    MessageBox.Show("Update reusit");
                    break;
                
                case 4:
                    query = @"UPDATE `resurse` SET Nume_Piesa = '" + textBox1.Text + "',Cantitate_disponibila = '" + textBox2.Text + "', Producator = '" + textBox3.Text + "', fisa_tehnica = '" +
                        textBox4.Text + "', distribuitor = '" + textBox5.Text + "', pret = '" + textBox6.Text + "' WHERE ID_piesa = '" + id.ToString() + "'; ";
                    //MessageBox.Show(query);// debug

                    DBConnect.execute_command(query);
                    MessageBox.Show("Update reusit !");
                    break;
                case 5:

                    break;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            // DELETE 
            switch (table_select)
            {
                case 0:
                    MessageBox.Show("Selectati o tabela !");
                    break;
                case 1:
                    // DELETE info  where ID = id (id selected by user clicking a row)
                    query = @"DELETE FROM membri WHERE ID = " + id.ToString();

                    DBConnect.execute_command(query);
                    //MessageBox.Show(query); //debug
                    MessageBox.Show("Inregistrarea a fost stearsa !");

                    break;

                case 2:
                    query = @"DELETE FROM `proiecte` WHERE ID_Proiect = " + id.ToString() + ";";

                    DBConnect.execute_command(query);
                    //MessageBox.Show(query); //debug
                    MessageBox.Show("Inregistrarea a fost stearsa !");

                    break;

                case 3:
                    query = @"DELETE FROM `echipe` WHERE ID_Membru = " + id.ToString();
                    //MessageBox.Show(query); //debug

                    DBConnect.execute_command(query);
                    MessageBox.Show("Inregistrarea a fost stearsa !");
                    break;

                case 4:
                    query = @"DELETE FROM `resurse`  WHERE ID_piesa = '" + id.ToString() + "'; ";
                    //MessageBox.Show(query);// debug

                    DBConnect.execute_command(query);
                    MessageBox.Show("Inregistrarea a fost stearsa !");
                    break;
                case 5:

                    break;
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        // select the table
        //the interface will change also

        /***************************************************************************************/


        private void membriToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (DBConnect.OpenConnection() == true)
            {
                mySqlDataAdapter = new MySqlDataAdapter(@"SELECT Nume, Prenume, Anul, Facultatea,Cod_Acces AS 'Cod Acces', Mail, NrTelefon 
                                                          FROM `membri` ORDER BY Nume", DBConnect.connection);
                DataSet DS = new DataSet();
                mySqlDataAdapter.Fill(DS);

                dataGridView1.DataSource = DS.Tables[0];
                //we have to tell the dataGridView to accept the changes
                //in order to ask for changes using the <_RowValidated> event
                ((DataTable)dataGridView1.DataSource).AcceptChanges();
                
                // set table_select to control inser/update/delet functions
                table_select = 1;

                // change interface
                label1.Text = " Membri";
                label2.Visible = true;
                label2.Text = "Nume";
                label3.Visible = true;
                label3.Text = "Prenume";
                label4.Visible = true;
                label4.Text = "Facultatea";
                label5.Visible = true;
                label5.Text = "An";
                label6.Visible = true;
                label6.Text = "Cod Acces";
                label7.Visible = true;
                label7.Text = "Mail";
                label8.Visible = true;
                label8.Text = "Nr. Telefon";

                textBox1.Visible = true;
                textBox2.Visible = true;
                textBox3.Visible = true;
                textBox4.Visible = true;
                textBox5.Visible = true;
                textBox6.Visible = true;
                textBox7.Visible = true;


                //close connection
                DBConnect.CloseConnection();
            }
        }

        private void proiecteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (DBConnect.OpenConnection() == true)
            {
                mySqlDataAdapter = new MySqlDataAdapter("SELECT nume,proiect,buget FROM `proiecte`,`membri` WHERE ID = Coordonator ORDER BY proiect;", DBConnect.connection);
                DataSet DS = new DataSet();
                mySqlDataAdapter.Fill(DS);

                dataGridView1.DataSource = DS.Tables[0];
                //we have to tell the dataGridView to accept the changes
                //in order to ask for changes using the <_RowValidated> event
                ((DataTable)dataGridView1.DataSource).AcceptChanges();

                // set table_select to control inser/update/delet functions
                table_select = 2;

                // change interface
                label1.Text = " Proiecte";
                label2.Visible = true;
                label2.Text = "Coordonator";
                label3.Visible = true;
                label3.Text = "Proiect";
                label4.Visible = true;
                label4.Text = "Buget";
                label5.Visible = false;
                label5.Text = "An";
                label6.Visible = false;
                label6.Text = "Cod Acces";
                label7.Visible = false;
                label7.Text = "Mail";
                label8.Visible = false;
                label8.Text = "Nr. Telefon";

                textBox1.Visible = true;
                textBox2.Visible = true;
                textBox3.Visible = true;
                textBox4.Visible = false;
                textBox5.Visible = false;
                textBox6.Visible = false;
                textBox7.Visible = false;

                //close connection
                DBConnect.CloseConnection();
            }
        }

        private void echipeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (DBConnect.OpenConnection() == true)
            {
                mySqlDataAdapter = new MySqlDataAdapter(@"SELECT Nume,Prenume,proiect FROM `membri`,`proiecte`,`echipe`
                                                          WHERE ID_Membru = ID AND proiecte.ID_Proiect = echipe.ID_Proiect ;", DBConnect.connection);
                DataSet DS = new DataSet();
                mySqlDataAdapter.Fill(DS);

                dataGridView1.DataSource = DS.Tables[0];
                //we have to tell the dataGridView to accept the changes
                //in order to ask for changes using the <_RowValidated> event
                ((DataTable)dataGridView1.DataSource).AcceptChanges();

                // set table_select to control inser/update/delet functions
                table_select = 3;

                // change interface
                label1.Text = " Echipe";
                label2.Visible = true;
                label2.Text = "Nume membru";
                label3.Visible = true;
                label3.Text = "Nume proiect";
                label4.Visible = false;
                label4.Text = "Buget";
                label5.Visible = false;
                label5.Text = "An";
                label6.Visible = false;
                label6.Text = "Cod Acces";
                label7.Visible = false;
                label7.Text = "Mail";
                label8.Visible = false;
                label8.Text = "Nr. Telefon";

                textBox1.Visible = true;
                textBox2.Visible = true;
                textBox3.Visible = false;
                textBox4.Visible = false;
                textBox5.Visible = false;
                textBox6.Visible = false;
                textBox7.Visible = false;

                //close connection
                DBConnect.CloseConnection();
            }
        }

        private void resurseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (DBConnect.OpenConnection() == true)
            {
                mySqlDataAdapter = new MySqlDataAdapter(@"select nume_piesa AS 'Nume Piesa', cantitate_disponibila AS 'Cantitate Disponibila', producator,
                                                         fisa_tehnica AS FisaTehnica, distribuitor, pret from resurse ORDER BY nume_piesa", DBConnect.connection);
                DataSet DS = new DataSet();
                mySqlDataAdapter.Fill(DS);

                dataGridView1.DataSource = DS.Tables[0];
                //we have to tell the dataGridView to accept the changes
                //in order to ask for changes using the <_RowValidated> event
                ((DataTable)dataGridView1.DataSource).AcceptChanges();

                // set table_select to control inser/update/delet functions
                table_select = 4;

                // change interface
                label1.Text = " Piese";
                label2.Visible = true;
                label2.Text = "Denumire piesa";
                label3.Visible = true;
                label3.Text = "Cantitate disponibila";
                label4.Visible = true;
                label4.Text = "Producator";
                label5.Visible = true;
                label5.Text = "Fisa tehnica";
                label6.Visible = true;
                label6.Text = "Distribuitor";
                label7.Visible = true;
                label7.Text = "Pret";
                label8.Visible = false;
                label8.Text = "Nr. Telefon";

                textBox1.Visible = true;
                textBox2.Visible = true;
                textBox3.Visible = true;
                textBox4.Visible = true;
                textBox5.Visible = true;
                textBox6.Visible = true;
                textBox7.Visible = false;

                //close connection
                DBConnect.CloseConnection();
            }
        }

        private void resurseConsumateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (DBConnect.OpenConnection() == true)
            {
                mySqlDataAdapter = new MySqlDataAdapter(@"SELECT nume_piesa AS 'Nume Piesa',proiect, Cantitate FROM `resurse_consumate`,`resurse`,`proiecte` 
                                                        WHERE resurse_consumate.ID_proiect = proiecte.ID_Proiect 
                                                        AND resurse.ID_Piesa = resurse_consumate.id_piesa ORDER BY nume_piesa", DBConnect.connection);
                DataSet DS = new DataSet();
                mySqlDataAdapter.Fill(DS);

                dataGridView1.DataSource = DS.Tables[0];
                //we have to tell the dataGridView to accept the changes
                //to be able to ask for changes using the <_RowValidated> event
                ((DataTable)dataGridView1.DataSource).AcceptChanges();

                // set table_select to control inser/update/delet functions
                table_select = 5;

                // change interface
                label1.Text = " Resurse Consumate";
                label2.Visible = true;
                label2.Text = "Proiect";
                label3.Visible = true;
                label3.Text = "Denumire Piesa";
                label4.Visible = true;
                label4.Text = "Cantitate";
                label5.Visible = false;
                label5.Text = "An";
                label6.Visible = false;
                label6.Text = "Cod Acces";
                label7.Visible = false;
                label7.Text = "Mail";
                label8.Visible = false;
                label8.Text = "Nr. Telefon";

                textBox1.Visible = true;
                textBox2.Visible = true;
                textBox3.Visible = true;
                textBox4.Visible = false;
                textBox5.Visible = false;
                textBox6.Visible = false;
                textBox7.Visible = false;

                //close connection
                DBConnect.CloseConnection();
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
        //display date in textBoxes when you click a row
        //used for update/delete
        private void dataGridView1_MouseClick(object sender, MouseEventArgs e)
        {
            switch (table_select)
            {
                case 1:
                    textBox1.Text = dataGridView1.SelectedRows[0].Cells[0].Value.ToString();
                    textBox2.Text = dataGridView1.SelectedRows[0].Cells[1].Value.ToString();
                    textBox3.Text = dataGridView1.SelectedRows[0].Cells[3].Value.ToString();
                    textBox4.Text = dataGridView1.SelectedRows[0].Cells[2].Value.ToString();
                    textBox5.Text = dataGridView1.SelectedRows[0].Cells[4].Value.ToString();
                    textBox6.Text = dataGridView1.SelectedRows[0].Cells[5].Value.ToString();
                    textBox7.Text = dataGridView1.SelectedRows[0].Cells[6].Value.ToString();

                    // get the ID of the row where user wants to UPDATE/DELETE info
                    query = @"SELECT ID FROM `membri` WHERE
                           nume = '" + textBox1.Text + "' AND prenume = '" + textBox2.Text + "' ;";
                    id = DBConnect.ReadID(query);

                    break;
                case 2:
                    textBox1.Text = dataGridView1.SelectedRows[0].Cells[0].Value.ToString();
                    textBox2.Text = dataGridView1.SelectedRows[0].Cells[1].Value.ToString();
                    textBox3.Text = dataGridView1.SelectedRows[0].Cells[2].Value.ToString();

                    // get the ID of the row where user wants to UPDATE/DELETE info
                    query = @"SELECT ID_Proiect FROM `proiecte` WHERE
                           proiect = '" + textBox2.Text + "' ;";
                    id = DBConnect.ReadID(query);
                    break;
                case 3:
                    // get index for update/delete
                    textBox1.Text = dataGridView1.SelectedRows[0].Cells[0].Value.ToString() + " " + dataGridView1.SelectedRows[0].Cells[1].Value.ToString();
                    textBox2.Text = dataGridView1.SelectedRows[0].Cells[2].Value.ToString();

                    //splite Name and Surename from textbox1 and keep the surename in temp[0]
                    temp = textBox1.Text.Split(' ');
                    query = @"SELECT ID_Membru FROM `echipe` WHERE ID_Membru = (SELECT ID FROM `membri` WHERE 
                            nume = '" + temp[0] + "')";

                    id = DBConnect.ReadID(query);
                    //MessageBox.Show(id.ToString());   //debug
                    break;
                case 4:
                    textBox1.Text = dataGridView1.SelectedRows[0].Cells[0].Value.ToString();
                    textBox2.Text = dataGridView1.SelectedRows[0].Cells[1].Value.ToString();
                    textBox3.Text = dataGridView1.SelectedRows[0].Cells[2].Value.ToString();
                    textBox4.Text = dataGridView1.SelectedRows[0].Cells[3].Value.ToString();
                    textBox5.Text = dataGridView1.SelectedRows[0].Cells[4].Value.ToString();
                    textBox6.Text = dataGridView1.SelectedRows[0].Cells[5].Value.ToString();

                    // get the ID of the row where user wants to UPDATE/DELETE info
                     query = @"SELECT ID_Piesa FROM `resurse` WHERE
                           nume_piesa = '" + textBox1.Text + "' ;";
                    id = DBConnect.ReadID(query);
                    //MessageBox.Show(id.ToString());  //debug
                    break;
                case 5:
                    textBox1.Text = dataGridView1.SelectedRows[0].Cells[1].Value.ToString();
                    textBox2.Text = dataGridView1.SelectedRows[0].Cells[0].Value.ToString();
                    textBox3.Text = dataGridView1.SelectedRows[0].Cells[2].Value.ToString();


                    break;
            }

        }

        private void complexQuerryToolStripMenuItem_Click(object sender, EventArgs e)
        {
         
        }

        private void proiecteScumpeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (DBConnect.OpenConnection() == true)
            {
                mySqlDataAdapter = new MySqlDataAdapter(@"SELECT proiect,SUM(Format((RC.Cantitate * R.pret),2)) AS COST,P.Buget FROM `resurse_consumate` RC,proiecte P,resurse R 
WHERE R.ID_Piesa = RC.ID_Piesa AND RC.ID_Proiect = P.ID_Proiect 
GROUP BY proiect
HAVING P.Buget < SUM(Format((RC.Cantitate * R.pret),2))", DBConnect.connection);
                DataSet DS = new DataSet();
                mySqlDataAdapter.Fill(DS);

                dataGridView1.DataSource = DS.Tables[0];
                //we have to tell the dataGridView to accept the changes
                //in order to ask for changes using the <_RowValidated> event
                ((DataTable)dataGridView1.DataSource).AcceptChanges();
                
                label1.Text = "Proiecte ce au depasit bugetul";

                DBConnect.CloseConnection();
            }
        }

        private void costProiecteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (DBConnect.OpenConnection() == true)
            {
                mySqlDataAdapter = new MySqlDataAdapter(@"SELECT proiect,SUM(Format((RC.Cantitate * R.pret),2)) AS COST FROM `resurse_consumate` RC,proiecte P,resurse R 
                                                          WHERE R.ID_Piesa = RC.ID_Piesa AND RC.ID_Proiect = P.ID_Proiect GROUP BY proiect ", DBConnect.connection);
                DataSet DS = new DataSet();
                mySqlDataAdapter.Fill(DS);

                dataGridView1.DataSource = DS.Tables[0];
                //we have to tell the dataGridView to accept the changes
                //in order to ask for changes using the <_RowValidated> event
                ((DataTable)dataGridView1.DataSource).AcceptChanges();

                label1.Text = "Costul fiecarui proiect";

                DBConnect.CloseConnection();
            }
        }

        private void membriFaraAccesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (DBConnect.OpenConnection() == true)
            {
                mySqlDataAdapter = new MySqlDataAdapter(@"SELECT P.proiect FROM membri M, proiecte P,echipe E WHERE (Cod_acces IS NULL OR Cod_acces = '') 
                                                    AND E.ID_membru = M.ID 
                                                    AND P.ID_proiect = E.ID_proiect 
GROUP BY proiect ", DBConnect.connection);
                DataSet DS = new DataSet();
                mySqlDataAdapter.Fill(DS);

                dataGridView1.DataSource = DS.Tables[0];
                //we have to tell the dataGridView to accept the changes
                //in order to ask for changes using the <_RowValidated> event
                ((DataTable)dataGridView1.DataSource).AcceptChanges();

                label1.Text = "Proiecte la care lucreaza membri ce nu au cod de acces";

                DBConnect.CloseConnection();
            }
        }

        private void coordonatoriToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (DBConnect.OpenConnection() == true)
            {
                mySqlDataAdapter = new MySqlDataAdapter(@"SELECT Nume, Prenume,Proiect FROM membri M,proiecte P, echipe E 
WHERE P.ID_Proiect = E.ID_Proiect AND P.Coordonator = M.ID 
                                  AND M.ID = E.ID_Membru ", DBConnect.connection);
                DataSet DS = new DataSet();
                mySqlDataAdapter.Fill(DS);

                dataGridView1.DataSource = DS.Tables[0];
                //we have to tell the dataGridView to accept the changes
                //in order to ask for changes using the <_RowValidated> event
                ((DataTable)dataGridView1.DataSource).AcceptChanges();

                label1.Text = "Coordonatori de proiecte";

                DBConnect.CloseConnection();
            }
        }

        private void situatieEchipeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (DBConnect.OpenConnection() == true)
            {
                mySqlDataAdapter = new MySqlDataAdapter(@"SELECT P.Proiect, COUNT(*)AS 'Numar Membri' FROM echipe E, proiecte P WHERE E.ID_Proiect = P.ID_Proiect GROUP BY E.ID_Proiect  ", DBConnect.connection);
                DataSet DS = new DataSet();
                mySqlDataAdapter.Fill(DS);

                dataGridView1.DataSource = DS.Tables[0];
                //we have to tell the dataGridView to accept the changes
                //in order to ask for changes using the <_RowValidated> event
                ((DataTable)dataGridView1.DataSource).AcceptChanges();

                label1.Text = "Numarul de membri din fiecare echipa";

                DBConnect.CloseConnection();
            }
        }

        private void situatieProiecteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (DBConnect.OpenConnection() == true)
            {
                mySqlDataAdapter = new MySqlDataAdapter(@"SELECT M.Nume, M.Prenume,COUNT(*) AS 'NrProiecte' FROM proiecte P, echipe E, membri M WHERE E.ID_Proiect = P.ID_Proiect AND E.ID_Membru = M.ID GROUP BY E.ID_Membru ", DBConnect.connection);
                DataSet DS = new DataSet();
                mySqlDataAdapter.Fill(DS);

                dataGridView1.DataSource = DS.Tables[0];
                //we have to tell the dataGridView to accept the changes
                //in order to ask for changes using the <_RowValidated> event
                ((DataTable)dataGridView1.DataSource).AcceptChanges();

                label1.Text = "Numarul de proiecte la care lucreaza fiecare membru";

                DBConnect.CloseConnection();
            }
        }

    }
// Create Form2.
    //use new form for input data from user

}
