Proiect BD

Rebega Alexandru
GRUPA 334AB


Proiectul is propune crearea unei baze de dat pentru gestionarea unui laborator de electronica.
Baza de date contine urmatoarele tabele:

Membri              - Contine informatiile personale a tutoror persoanelor ce au acces in laborator
                    -ID - cheie primara, Nume, Prenume, Mail, Nr. Telefon, Anul, Facultatea, Codul de acces(daca exista)
      
Proiecte            - ID proiect - cheie primara, cheie externa pentru `Echipe`, `Resurse Consumate`
                    - ID Coordonator - cheie externa - relatie One-to-many
                    - Proiect(nume proiect)
                    -Buget

Resurse             - Informatii pespre piesele disponibile
                    - ID Piesa - cheie primara
                    - Cantitate dispinibila, Producator, Fisa Tehnica, Distribuitor, Pret

Echipe              - Tabela de legatura - Membri - Proiecte - Relatie Many-to-many
                    - Contine ID membru, ID Proiect

Resurse Consumate   - Cheie primara multipla formata din doua chei externe: ID Proiect si ID piesa
                    - Relatie Many-to-Many cu tabela `Proiecte`
                    - Campuri : Id piesa, Id Proiect, Cantitate

Observatii:

*Integritatea informatiilor din baza de date este asigurata de constrangerile PRYMARY, FOREIGN KEY, NOT NULL;
*Baza de date respecte formele normale FN1, FN2, FN3;
*Proiectul contine o baza de date MySQL, gestionata de un server Apache local, creat cu ajutorul softului XAMPP
*Interfata a fost realizata cu in limbaju C#. Contine un DataGridView pentru afisarea informatiilor obtinute
prin interogari ale bazei de date, realizate cu ajutorul clasei MySQLDataAdapter;

Din meniul 'Vizualizare' se alege tabela ce urmeaza a fi vizualizata in DataGridView, asadar alege interogarea 
trimisa bazei de date.

La evenimentul 'Clicl_on_Row' specific DataGridView, se face o interogare prin care se determina ID-ul(respectiv cheia primara) 
specifica tabelei afisate in DataGridView, utilizat in operatiile UPDATE/ DELETE. 

Pentru input/output interfata contine Texboxuri si Labeluri dinamice.

Lista interogarilor folosite de interfata C# :
<query_list>
	
INSERT (include subcerere SELECT):

	query = "INSERT INTO `membri` (`ID`, `Nume`, `Prenume`, `Facultatea`, `Anul`, `Cod_acces`, `Mail`, `NrTelefon`) 
			 VALUES (NULL, '" + info[0] + "','" + info[1] + "','" + info[2] +
    				"','" + info[3] + "','" + info[4] + "','" + info[5] + "','" + info[6] + "');";

    query = @"INSERT INTO `proiecte` (`ID_Proiect`, `Coordonator`, `Proiect`, `Buget`) 
    		  VALUES (NULL, (SELECT ID FROM `membri` WHERE nume ='" + info[0] + "'),
    		  				'" + info[1] + "','" + info[2] + "');";

    query = @"INSERT INTO `echipe` (`ID_Membru`, `ID_Proiect`) 
              VALUES ((SELECT ID FROM `membri` 
              WHERE nume = '" + info[0] + "'),
              				(SELECT ID_Proiect FROM `proiecte` WHERE proiect ='" + info[1] + "'));";

    query = @"INSERT INTO `resurse` (`ID_Piesa`, `Nume_piesa`, `Cantitate_disponibila`, `Producator`, `Fisa_tehnica`, `Distribuitor`, `Pret`) 
              VALUES (NULL, '" + info[0] + "','" + info[1] + "','" + info[2] +
                      "','" + info[3] + "','" + info[4] + "','" + info[5] + "');";

    query = @"INSERT INTO `resurse_consumate` (`ID_Proiect`, `ID_piesa`, `Cantitate`) 
              VALUES ((SELECT ID_Proiect FROM `proiecte` WHERE proiect = '" + info[0] + "'),
              		  (SELECT ID_Piesa FROM `resurse` WHERE Nume_piesa ='" + info[1] + "'),'" + info[2] + "');";
UPDATE (include subcerere SELECT):

	query = @"UPDATE `membri` 
			  SET nume = '" + textBox1.Text +
                        "', prenume = '" + textBox2.Text + "',facultatea = '" + textBox3.Text + "', anul = '"
                        + textBox4.Text + "', cod_acces = '" + textBox5.Text + "', mail = '" + textBox6.Text +
                        "', nrtelefon = '" + textBox7.Text + "' 
              WHERE ID = " + id.ToString();

    query = @"UPDATE `proiecte` 
    		  SET `Coordonator` = (SELECT ID FROM `membri` WHERE nume ='" + textBox1.Text + "'), 
    		  					  Proiect = '" + textBox2.Text + "', Buget = '" + textBox3.Text +"' 
    		  WHERE ID_Proiect = " + id.ToString() + ";";

    query = @"UPDATE `echipe` 
    		  SET ID_membru = (SELECT ID from `membri` WHERE nume = '" + temp[0] + "' AND prenume = '" + temp[1] + "'), 
    		  	  ID_Proiect = (SELECT ID_Proiect FROM `proiecte` WHERE proiect = '" + textBox2.Text + "')";

    query = @"UPDATE `resurse` 
    		  SET Nume_Piesa = '" + textBox1.Text + "',Cantitate_disponibila = '" + textBox2.Text + "', Producator = '" 
    		  					  + textBox3.Text + "', fisa_tehnica = '" + textBox4.Text + "', distribuitor = '" + textBox5.Text + "', pret = '" + textBox6.Text + "' 
    		WHERE ID_piesa = '" + id.ToString() + "'; ";  

DELETE:

	query = @"DELETE FROM membri WHERE ID = " + id.ToString();  

	query = @"DELETE FROM `proiecte` WHERE ID_Proiect = " + id.ToString() + ";";

	query = @"DELETE FROM `echipe` WHERE ID_Membru = " + id.ToString();

	query = @"DELETE FROM `resurse`  WHERE ID_piesa = '" + id.ToString() + "'; ";

SELECT:

	mySqlDataAdapter = new MySqlDataAdapter("SELECT Nume, Prenume, Anul, Facultatea,Cod_Acces AS 'Cod Acces', Mail, NrTelefon FROM `membri` ", 					       DBConnect.connection);

	mySqlDataAdapter = new MySqlDataAdapter("SELECT nume,proiect,buget FROM `proiecte`,`membri` 
											 WHERE ID = Coordonator ;", 
				           DBConnect.connection);

	mySqlDataAdapter = new MySqlDataAdapter(@"SELECT Nume,Prenume,proiect FROM `membri`,`proiecte`,`echipe`
                                              WHERE ID_Membru = ID AND proiecte.ID_Proiect = echipe.ID_Proiect ;", 
                           DBConnect.connection);

    mySqlDataAdapter = new MySqlDataAdapter(@"select nume_piesa AS 'Nume Piesa', 
    										  cantitate_disponibila AS 'Cantitate Disponibila', producator,
                                              fisa_tehnica AS FisaTehnica, distribuitor, pret from resurse",
                           DBConnect.connection);    

    mySqlDataAdapter = new MySqlDataAdapter(@"SELECT nume_piesa AS 'Nume Piesa',proiect, Cantitate 
    										  FROM `resurse_consumate`,`resurse`,`proiecte` 
                                              WHERE resurse_consumate.ID_proiect = proiecte.ID_Proiect 
                                              AND resurse.ID_Piesa = resurse_consumate.id_piesa ", 
                           DBConnect.connection);            

    query = @"SELECT ID FROM `membri` 
    		  WHERE nume = '" + textBox1.Text + "' AND prenume = '" + textBox2.Text + "' ;";

    query = @"SELECT ID_Proiect 
    		  FROM `proiecte` 
    		  WHERE proiect = '" + textBox2.Text + "' ;";

    query = @"SELECT ID_Membru FROM `echipe` 
              WHERE ID_Membru = (SELECT ID 
                                 FROM `membri` 
              					 WHERE nume = '" + temp[0] + "')";

    query = @"SELECT ID_Piesa FROM `resurse` 
    		  WHERE nume_piesa = '" + textBox1.Text + "' ;";


	id = DBConnect.ReadID(query);

INTEROGARI COMPLEXE:

******************************************************************************************************************************************
Costul fiecarui proiect

SELECT proiect,SUM(Format((RC.Cantitate * R.pret),2)) AS COST FROM `resurse_consumate` RC,proiecte P,resurse R WHERE R.ID_Piesa = RC.ID_Piesa AND RC.ID_Proiect = P.ID_Proiect GROUP BY proiect 

******************************************************************************************************************************************
Proiecte la care lucreaza membri ce nu au cod acces

SELECT P.proiect FROM membri M, proiecte P,echipe E WHERE (Cod_acces IS NULL OR Cod_acces = '') 
                                                    AND E.ID_membru = M.ID 
                                                    AND P.ID_proiect = E.ID_proiect 
GROUP BY proiect

******************************************************************************************************************************************
Proiecte ce au depasit bugetul

SELECT proiect,SUM(Format((RC.Cantitate * R.pret),2)) AS COST,P.Buget FROM `resurse_consumate` RC,proiecte P,resurse R 
WHERE R.ID_Piesa = RC.ID_Piesa AND RC.ID_Proiect = P.ID_Proiect 
GROUP BY proiect
HAVING P.Buget < SUM(Format((RC.Cantitate * R.pret),2))
******************************************************************************************************************************************
Coordonatori de proiecte

SELECT Nume, Prenume,Proiect FROM membri M,proiecte P, echipe E 
WHERE P.ID_Proiect = E.ID_Proiect AND P.Coordonator = M.ID 
                                  AND M.ID = E.ID_Membru 
******************************************************************************************************************************************       
Numar membri fiecare echipa

SELECT P.Proiect, COUNT(*)AS 'Numar Membri' FROM echipe E, proiecte P WHERE E.ID_Proiect = P.ID_Proiect GROUP BY E.ID_Proiect                            
******************************************************************************************************************************************
Nr proiecte la care lucreaza fiecare membru

SELECT M.Nume, M.Prenume,COUNT(*) AS 'NrProiecte' FROM proiecte P, echipe E, membri M WHERE E.ID_Proiect = P.ID_Proiect AND E.ID_Membru = M.ID GROUP BY E.ID_Membru 

</query_list>
