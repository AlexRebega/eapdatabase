-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 11 Ian 2016 la 09:52
-- Versiune server: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laboratorelectronica`
--

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `echipe`
--

CREATE TABLE `echipe` (
  `ID_Membru` int(11) NOT NULL,
  `ID_Proiect` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Salvarea datelor din tabel `echipe`
--

INSERT INTO `echipe` (`ID_Membru`, `ID_Proiect`) VALUES
(1, 1),
(2, 3),
(12, 1),
(16, 8);

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `membri`
--

CREATE TABLE `membri` (
  `ID` int(11) NOT NULL,
  `Nume` varchar(30) NOT NULL,
  `Prenume` varchar(30) NOT NULL,
  `Facultatea` char(30) DEFAULT NULL,
  `Anul` int(11) DEFAULT NULL,
  `Cod_acces` varchar(10) DEFAULT NULL,
  `Mail` varchar(30) DEFAULT NULL,
  `NrTelefon` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Salvarea datelor din tabel `membri`
--

INSERT INTO `membri` (`ID`, `Nume`, `Prenume`, `Facultatea`, `Anul`, `Cod_acces`, `Mail`, `NrTelefon`) VALUES
(1, 'Rebega', 'Alexandru', 'AII', 3, '144019', 'alex.rebega@gmail.com', '0786148019'),
(2, 'Ghilinta', 'Daniel', 'AII', 2, '113429', 'daniel.chilinta@yahoo.com', '0722343119'),
(3, 'Patru', 'Cristian', 'AII', 4, '922433', 'cristi.patru@aut.pub.ro', '0744112343'),
(4, 'Aungurencei', 'Alex', 'AII', 5, '922103', 'alex_aungurencei@gmail.com', '0732114024'),
(6, 'Tiron', 'Anamaria', 'AII', 3, NULL, 'tiron.ana@yahoo.com', '0723149017'),
(7, 'Partin', 'Csaba', 'AII', 3, NULL, 'Partin.Csaba@yahoo.com', '0782119013'),
(8, 'Ilie Ablachim', 'Denis Constantin', 'AII', 3, '073119', 'iliia.deni@yahohu.com', '0755110232'),
(9, 'Stoican', 'Cristi', 'AII', 4, '102339', 'stoican.cristi@yahoo.com', '0788149002'),
(10, 'Mateisca', 'Iulian', 'AII', 3, '452192', 'Iulian.Mateisca@yahoo.com', '0792112042'),
(11, 'Calciu', 'Iulian-Cosmin', 'AII', 5, '', 'iulian.calciu@yahoo.com', '0743210019'),
(12, 'Frigioiu', 'Stefan', 'AII', 3, '113429', 'frigioiu.stefan@yahoo.com', '0727343119'),
(16, 'Banu', 'Calin', 'AII', 3, '', 'banu@gmail.com', '0784119253');

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `proiecte`
--

CREATE TABLE `proiecte` (
  `ID_Proiect` int(11) NOT NULL,
  `Coordonator` int(11) NOT NULL,
  `Proiect` varchar(30) NOT NULL,
  `Buget` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Salvarea datelor din tabel `proiecte`
--

INSERT INTO `proiecte` (`ID_Proiect`, `Coordonator`, `Proiect`, `Buget`) VALUES
(1, 1, 'Linefollower', NULL),
(2, 3, 'Maze Robot', 110),
(3, 2, 'TeslaCoil', 240),
(4, 10, 'MazeRobot', 120),
(8, 16, 'PanouSolarInteligent', 300);

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `resurse`
--

CREATE TABLE `resurse` (
  `ID_Piesa` int(11) NOT NULL,
  `Nume_piesa` varchar(30) NOT NULL,
  `Cantitate_disponibila` int(11) DEFAULT NULL,
  `Producator` varchar(30) DEFAULT NULL,
  `Fisa_tehnica` varchar(100) DEFAULT NULL,
  `Distribuitor` varchar(30) DEFAULT NULL,
  `Pret` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Salvarea datelor din tabel `resurse`
--

INSERT INTO `resurse` (`ID_Piesa`, `Nume_piesa`, `Cantitate_disponibila`, `Producator`, `Fisa_tehnica`, `Distribuitor`, `Pret`) VALUES
(1, 'Rezistenta 1K', 200, 'LT', NULL, 'commet', 0.23),
(2, 'LM7805', 146, 'LT', 'www.datasheets.com/', 'farnell', 1.02),
(3, 'drv8866', 26, 'TI', 'www.datasheets.com/', 'farnell.com', 5.02),
(4, 'Atmega32u4', 4, 'Atmel', 'www.datasheets.com/avr', 'commet.srl.com/', 4.48),
(5, 'LM7806', 25, 'NXP', 'www.datasheets.com/', 'commet.srl.com/', 1.43),
(6, 'TPS2987', 3, 'TexasInstruments', 'www.datasheets.com/tps2987', 'www.farnell.com', 5.73),
(7, 'TPS2887', 3, 'TexasInstruments', 'www.datasheets.com/tps2887', 'www.farnell.com', 5.73);

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `resurse_consumate`
--

CREATE TABLE `resurse_consumate` (
  `ID_Proiect` int(11) NOT NULL,
  `ID_piesa` int(11) NOT NULL,
  `Cantitate` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Salvarea datelor din tabel `resurse_consumate`
--

INSERT INTO `resurse_consumate` (`ID_Proiect`, `ID_piesa`, `Cantitate`) VALUES
(1, 1, 1),
(1, 3, 2),
(2, 2, 1),
(3, 1, 17),
(1, 3, 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `echipe`
--
ALTER TABLE `echipe`
  ADD PRIMARY KEY (`ID_Membru`,`ID_Proiect`);

--
-- Indexes for table `membri`
--
ALTER TABLE `membri`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `proiecte`
--
ALTER TABLE `proiecte`
  ADD PRIMARY KEY (`ID_Proiect`);

--
-- Indexes for table `resurse`
--
ALTER TABLE `resurse`
  ADD PRIMARY KEY (`ID_Piesa`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `membri`
--
ALTER TABLE `membri`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `proiecte`
--
ALTER TABLE `proiecte`
  MODIFY `ID_Proiect` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `resurse`
--
ALTER TABLE `resurse`
  MODIFY `ID_Piesa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
